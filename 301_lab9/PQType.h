// Definition of class PQType, which represents the Priority Queue ADT
class FullPQ{};
class EmptyPQ{};
#include "Heap.h"
template<class ItemType>
class PQType
{
public:
  PQType(int);          // parameterized class constructor
  ~PQType();            // class destructor

  void MakeEmpty();
  // Function: Initializes the queue to an empty state.
  // Post: Queue is empty.

  bool IsEmpty() const;
  // Function: Determines whether the queue is empty.
  // Post: Function value = (queue is empty)

  bool IsFull() const;
  // Function: Determines whether the queue is full.
  // Post: Function value = (queue is full)

  void Enqueue(ItemType newItem);
  // Function: Adds newItem to the rear of the queue.
  // Post: if (the priority queue is full) exception FullPQ is thrown;
  //       else newItem is in the queue.

  void Dequeue(ItemType& item);
  // Function: Removes element with highest priority from the queue
  // and returns it in item.
  // Post: If (the priority queue is empty) exception EmptyPQ is thrown;
  //       else highest priority element has been removed from queue.
  //       item is a copy of removed element.
private:
  int length;
  HeapType<ItemType> items;
  int maxItems;
};

template<class ItemType>
PQType<ItemType>::PQType(int max)
{
  maxItems = max;
  items.elements = new ItemType[max];
  length = 0;
}

template<class ItemType>
void PQType<ItemType>::MakeEmpty()
{
  length = 0;
}

template<class ItemType>
PQType<ItemType>::~PQType()
{
  delete [] items.elements;
}

//I used C++ style braces instead of java, just to match this assignment :P
template<class ItemType>
bool PQType<ItemType>::IsEmpty() const
{
// Function: Determines whether the queue is empty.
// Post: Function value = (queue is empty)

  return 0 == length;
}

template<class ItemType>
bool PQType<ItemType>::IsFull() const
{
// Function: Determines whether the queue is full.
// Post: Function value = (queue is full)

  return length >= maxItems;//shouldn't need >=, == should work
                            // ...but >= feels safer
}

template<class ItemType>
void PQType<ItemType>::Enqueue(ItemType newItem)
{
// Function: Adds newItem to the rear of the queue.
// Post: if (the priority queue is full) exception FullPQ is thrown;
//       else newItem is in the queue.

  if(IsFull()){
    FullPQ a_problem;
    throw(a_problem);
  }
  //else
  items.elements[length] = newItem;// empty/garbage spot at location (length)
  items.ReheapUp(0, length);       //normally (length-1), but we didn't increment yet
  length++;                        //increment to hold new item count
}

template<class ItemType>
void PQType<ItemType>::Dequeue(ItemType& item)
{
// Function: Removes element with highest priority from the queue
// and returns it in item.
// Post: If (the priority queue is empty) exception EmptyPQ is thrown;
//       else highest priority element has been removed from queue.
//       item is a copy of removed element.

  if(IsEmpty()){
    EmptyPQ a_problem;
    throw(a_problem);
  }
  //else
  item = items.elements[0];                  //soon to be removed element is now saved in item
  length--;                                  //move back to point at the final item
  items.elements[0] = items.elements[length];//swap it to the front
  items.ReheapDown(0, length-1);             //last element is at location (length-1)
//item at location [length] is now garbage data
}

