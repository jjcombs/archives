template <class ItemType>
void Swap(ItemType& one, ItemType& two);

// Assumes ItemType is either a built-in simple type or a class
// with overloaded relational operators.
template <class ItemType>
struct HeapType
{
  void ReheapDown(int root, int bottom);
  void ReheapUp(int root, int bottom);
  ItemType* elements;   // Array to be allocated dynamically
  int numElements;
};

template <class ItemType>
void Swap(ItemType& one, ItemType& two)
{
  ItemType temp;
  temp = one;
  one = two;
  two = temp;
}

template<class ItemType>
void HeapType<ItemType>::ReheapUp(int root, int bottom)
// Post: Heap property is restored.
{
//we just appended an element to the list
//now we need to move the newly added item to an appropriate place in the heap

    //first, we need to check if our current node's parent is greater
    int parent_of_bottom = (bottom - 1) / 2; //-1 will remove the +1 for
    // the left child and cause a +2 from the right child to get truncated away
    if(bottom == root || // if we're checking the root, we're done
       elements[parent_of_bottom] > elements[bottom])
        return; // we're done!
    //else
    //if we didn't return, we need to swap
    Swap(elements[parent_of_bottom], elements[bottom]);
    //and keep going
    ReheapUp(root, parent_of_bottom);
}

template<class ItemType>
void HeapType<ItemType>::ReheapDown(int root, int bottom)
// Post: Heap property is restored.
{
//we just swapped the first and last element
//now we need to swap the root number down to an appropriate node

    //first, we need to check if our currently targeted node has a larger
    // value than both of its children, we can compare with the larger one
    int larger_child = root * 2 + 1;//assume left child is larger
    if(larger_child > bottom)
        return;//no children! we're already a leaf node! DONE!

    if(larger_child < bottom &&//make sure that there exists a right child
        elements[larger_child] < elements[larger_child + 1])//is it bigger?
        larger_child++;//pick right child, if it's larger
    //yes it's only one line after the condition, it's a big condition...

    if(elements[root] >= elements[larger_child])
        return;//we're larger than or equal, yay! DONE!
    //else unneeded
    //if we haven't returned from being done, we need to swap the parent
    // with its larger child
    Swap(elements[root],elements[larger_child]);
    //and keep going until we stop early due to a base case:
    ReheapDown(larger_child, bottom);
}
