h9q4 [x] = if x `mod` 2 == 1 then x:[]
           else []
h9q4 xs = if (head xs) `mod` 2 == 1 then (head xs):(h9q4 (tail xs))
          else h9q4 (tail xs)
