h9q5 xs i = if i == 0 then head xs
          else h9q5 (tail xs) (i - 1)
