	.data
stackSpaceNeeded = word*2
	.code
main:	addi	$sp,$sp,-stackSpaceNeeded	#room for input/output
	li	$a0,'\f
	syscall	$print_char	#clear screen for testing

	li	$a0,50
	sw	$a0,0($sp)
	jal	Fibonacci
	lw	$a0,4($sp)	#load E
	syscall	$print_int	#-1 if N > 47
	li	$a0,'\n
	syscall	$print_char


	li	$a0,40
	sw	$a0,0($sp)
	jal	Fibonacci
	lw	$a0,4($sp)	#load E
	syscall	$print_int	#-1 if N > 47
	li	$a0,'\n
	syscall	$print_char

	li	$a0,5
	sw	$a0,0($sp)
	jal	Fibonacci
	lw	$a0,4($sp)	#load E
	syscall	$print_int	#-1 if N > 47
	li	$a0,'\n
	syscall	$print_char

	li	$a0,47
	sw	$a0,0($sp)
	jal	Fibonacci
	lw	$a0,4($sp)	#load E
	syscall	$print_int	#-1 if N > 47
	li	$a0,'\n
	syscall	$print_char

	li	$a0,20
	sw	$a0,0($sp)
	jal	Fibonacci
	lw	$a0,4($sp)	#load E
	syscall	$print_int	#-1 if N > 47
	li	$a0,'\n
	syscall	$print_char


	addi	$sp,$sp,stackSpaceNeeded	#clean up stack
	syscall	$exit
################################################################################
# void Fibonacci (int & N:0($sp), int & E:4($sp))
# arguments:
#	N:	0($sp):  request for Nth Fibonacci number
#	E:	4($sp):  reserved for return value
# returns:
#	E:	4($sp):  Nth Fibonacci number or -1 if N > 47
# uses:
#	top (2) spots of (given) stack
#	t0
#	t1
#	t2
#	t3
# calls:
#	[this is a leaf function]
#-------------------------------------------------------------------------------
# pseudocode
#	int t0 = N;
#	int t3 = -1;
#	if(t0 > 47 || t0 <= 0){
#		E = t3;
#		return;
#	}
#	if(t0 <= 2){
#		E = t0 + t3;
#		return;
#	}
#	int t1 = 0;
#	int t2 = 1;
#	t0 -= 2;
#	do{
#		t3 = t2 + t1;
#		t1 = t2;
#		t2 = t3;
#		t0--;
#	}while(t0 > 0);
#	E = t3;
#	return;
#-------------------------------------------------------------------------------
Fibonacci:
	lw	$t0,0($sp)	#int t0 = N;
	li	$t3,-1		#int t3 = -1;
	bgtu	$t0,47,99f	#if(t0 > 47 || t0 < 0){E = t3; return;}
	bgtu	$t0,1,1f	#if(t0 <= 2){E = t0 + t3;return;}
	add	$t3,$t3,$t0	
	b	99f
1:	li	$t1,0		#int t1 = 0;
	li	$t2,1		#int t2 = 1;
	addi	$t0,$t0,-2	#t0 -= 2;
2:	add	$t3,$t2,$t1	#do{	t3 = t2 + t1;
	mov	$t1,$t2		#	t1 = t2;
	mov	$t2,$t3		#	t2 = t3;
	addi	$t0,$t0,-1	#	t0--;
	bgtz	$t0,2b		#}while(t0 > 0);
99:	sw	$t3,4($sp)	#E = t3;
	jr	$ra		#return;