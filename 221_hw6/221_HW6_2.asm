	.data
arrayX:	.ascii	"abcdefghijklmnopqrstuvwxyz"
arrayX_end:
#consant that will be our N:
sizeN	= arrayX_end-arrayX
sizeOfInput = 3*word
	.code
main:	addi	$sp,$sp,-sizeOfInput	#room for (&X, N, V)
	li	$a0,'\f
	syscall	$print_char
	li	$s0,arrayX
	sw	$s0,0($sp)	#store &X
	li	$s1,sizeN
	sw	$s1,4($sp)	#store N
	li	$s2,'A		#A not present, print -1
	sw	$s2,8($sp)
	jal	Search
	mov	$a0,$v0
	syscall	$print_int
	li	$a0,'\n
	syscall	$print_char
	li	$s2,'a		#find a (spot: 1-1=0)
	sw	$s2,8($sp)
	jal	Search
	mov	$a0,$v0
	syscall	$print_int
	li	$a0,'\n
	syscall	$print_char
	li	$s2,'z		#find z (spot: 26-1=25)
	sw	$s2,8($sp)
	jal	Search
	mov	$a0,$v0
	syscall	$print_int
	addi	$sp,$sp,sizeOfInput	#clean up stack
	syscall	$exit
################################################################################
# int Search (&X:0($sp), N:4($sp), V:8($sp),)
# arguments:
#	&X:0($sp):  byte array X's address
#	 N:4($sp):  number of bytes in array X
#	 V:8($sp):  value whose index we search
# returns:
#	v0:	the (largest) index whose value is V
# uses:
#	(given) stack
#	t0
#	t1
#	t2
#	v0
# calls:
#	[this is a leaf function]
#-------------------------------------------------------------------------------
# pseudocode
#	char* t0 = &X;
#	char* v0 = t0 + N;
#	char t1 = V;
#	char t2;
#	while((--v0) >= t0){
#		t2 = *v0;
#		if(t2 == t1)
#			break;
#	}
#	return v0;
#-------------------------------------------------------------------------------
Search:	lw	$t0,0($sp)	#load addresses
	lw	$v0,4($sp)	#no error checking! T_T
	lb	$t1,8($sp)	#value to compare with
	add	$v0,$t0,$v0	#v0 points past the end of X
1:	addi	$v0,$v0,-1	#point to next element
	blt	$v0,$t0,2f	#while(v0 >= &X){
	lb	$t2,0($v0)	#load element
	bne	$t2,$t1,1b	#keep looking if not equal
2:	sub	$v0,$v0,$t0	#normalize v0 for return
	jr	$ra