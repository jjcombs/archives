	.data
arrayX:	.word	0:5
arrayY:	.word	5,10,-85,20,25
arrayZ:	.word	-95,-90,15,-30,-25
#consant that will be our N:
sizeN	= arrayY-arrayX
stackSpaceNeeded = word*5
	.code
main:	addi	$sp,$sp,-stackSpaceNeeded	#room for input/output
	li	$a0,'\f
	syscall	$print_char	#clear screen for testing

	li	$s0,arrayX
	sw	$s0,0($sp)	#store &X
	li	$s1,arrayY
	sw	$s1,4($sp)	#store &Y
	li	$s2,arrayZ
	sw	$s2,8($sp)	#store &Z
	li	$s3,sizeN
	sw	$s3,12($sp)	#store N

	jal	AVA
	add	$t9,$s3,$s0
	mov	$t0,$s0	
1:	lw	$a0,0($t0)	#cout << X[i]
	sw	$0,0($t0)	#reset X
	syscall	$print_int
	li	$a0,'\n
	syscall	$print_char
	addi	$t0,$t0,word
	bgt	$t9,$t0,1b
	lw	$a0,16($sp)	#print status
	syscall	$print_int
	li	$a0,'\n
	syscall	$print_char

	li	$t0,11001001000	#cause overflow!
	sw	$t0,word($s1)	#test the status!
	sw	$t0,word($s2)
	syscall	$print_char
	syscall	$print_char
	jal	AVA


	mov	$t0,$s0	
1:	lw	$a0,0($t0)	#cout << X[i]
	sw	$0,0($t0)	#reset X
	syscall	$print_int
	li	$a0,'\n
	syscall	$print_char
	addi	$t0,$t0,word
	bgt	$t9,$t0,1b
	lw	$a0,16($sp)	#print status
	syscall	$print_int
	li	$a0,'\n
	syscall	$print_char

	addi	$sp,$sp,stackSpaceNeeded	#clean up stack
	syscall	$exit
################################################################################
# void AVA (&X:0($sp), &Y:4($sp), &Z:8($sp), N:12($sp), status:16($sp))
# arguments:
#	&X:	0($sp):  int array X's address (used for returning values)
#	&Y:	4($sp):  int array Y's address
#	&Z:	8($sp):  int array Z's address
#	N:	12($sp): number of words in arrays X, Y, and Z
#	status:	16($sp): reserved for return value
# returns:
#	&X:	0($sp):  int array X contains the
#			 absolute value vector addition of Y and Z
#	status:	16($sp): 1 if arithmatic overflow occurred, 0 otherwise
# uses:
#	top (5) spots of (given) stack
#	t0
#	t1
#	t2
#	t3
#	t4
#	t5
#	t6
# calls:
#	[this is a leaf function]
#-------------------------------------------------------------------------------
# pseudocode
#	int* t0 = &X;
#	int* t1 = t0 + N;
#	int* t2 = &Y + N;
#	int* t3 = &Z + N;
#	int t4, t5, t6;
#	do{
#		t5 = *(--t2);
#		t6 = *(--t3);
#		if(t5 < 0)
#			t5 = -t5;
#		if(t6 < 0)
#			t6 = -t6;
#		t4 = t5 + t6;
#		if(t4 < 0){
#			status = 1;
#			return;
#		}
#		*(--t1) = t4;
#	}while(t1 > t0);
#	status = 0;
#	return;
#-------------------------------------------------------------------------------
AVA:	lw	$t0,0($sp)	#load address	&X
	lw	$t2,4($sp)	#load address	&Y
	lw	$t3,8($sp)	#load address	&Z
	lw	$t1,12($sp)	#load size	N
	add	$t2,$t2,$t1	#point past end of Y
	add	$t3,$t3,$t1	#, Z
	add	$t1,$t0,$t1	#, and X
1:	addi	$t2,$t2,-word	#do{	t5 = *(--t2);
	lw	$t5,0($t2)
	addi	$t3,$t3,-word	#	t6 = *(--t3);
	lw	$t6,0($t3)
	bgez	$t5,2f		#	if(t5 < 0)
	sub	$t5,$0,$t5	#		t5 = -t5;
2:	bgez	$t6,3f		#	if(t6 < 0)
	sub	$t6,$0,$t6	#		t6 = -t6;
3:	addu	$t4,$t5,$t6	#	t4 = t5 + t6;
	bgez	$t4,4f		#	if(t4 < 0){
	addi	$t0,$0,1
	sw	$t0,16($sp)	#		status = 1;
	jr	$ra		#		return;
4:	addi	$t1,$t1,-word	#	*(--t1) = t4;
	sw	$t4,0($t1)
	bgt	$t1,$t0,1b	#}while(t1 > t0);
	sw	$0,16($sp)	#status = 0;
	jr	$ra		#return;