	.code
main:	li	$a0,'\f
	syscall	$print_char	#clear screen for testing

	syscall $read_int
	mov	$a1,$v0		#cin >> J:a1
	syscall $read_int
	mov	$a2,$v0		#cin >> K:a2
	syscall $read_int
	mov	$a3,$v0		#cin >> L:a3

	jal	chico
	add	$a0,$a2,$a3	#M = K + L;
	sub	$a0,$a1,$a0	#M = J - M;
	syscall	$print_int	#cout << M;

	syscall	$exit

#use a1,a2,a3 for inputs to save a clock cycle on making/printing M later
chico:	sll	$a1,$a1,3	#*X * 8;

	sra	$t2,$a2,2	#t2 = Y/4;

	sll	$t3,$a3,3	#t3 = Z * 8;
	add	$t3,$a3,$t3	#t3 += Z;
	add	$t3,$a3,$t3	#t3 += Z; // now: t3 = Z * 10

	sub	$a1,$a1,$t3	#*X -= t3; //Z*10
	add	$a1,$t2,$a1	#*X += t2; //Y/4

	jr	$ra		#return;