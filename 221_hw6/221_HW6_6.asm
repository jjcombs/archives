	.data
wowhax:	.ascii	" cycles (+ a few cycles overhead) to run BubSort!\n"
	.asciiz	"Array size used: "
arrayX:	.word	1:10000
arrayX_end:
#consant that will be our N:
sizeN	= arrayX_end-arrayX
stackSpaceNeeded = word*2
	.code
main:	addi	$sp,$sp,-stackSpaceNeeded	#room for input/output
	li	$a0,'\f
	syscall	$print_char	#clear screen for testing

	la	$s0,arrayX
	sw	$s0,0($sp)	#store &X
	li	$s1,sizeN
	sw	$s1,4($sp)	#store N

#initialize array to be sorted in descending order so it'll be obvious when
#it's sorted
	add	$t0,$s0,$s1
	add	$t1,$0,$0
1:	addi	$t0,$t0,-word	#from 9999 down to 0
	sw	$t1,0($t0)
	addi	$t1,$t1,1
	bgt	$t0,$s0,1b

	syscall	$random		#number of cycles to complete sub-program
	mov	$s2,$v0		# +1
	jal	BubSort		#jal +1?
	syscall	$random		# maybe +"syscall $random" overhead? (1?)
	sub	$a0,$s2,$v0
	syscall	$print_int

	li	$a0,wowhax
	syscall	$print_string
	mov	$a0,$s1
	syscall	$print_int

	addi	$sp,$sp,stackSpaceNeeded	#clean up stack
	syscall	$exit
################################################################################
# void BubSort (&X:0($sp), N:4($sp))
# arguments:
#	&X:	0($sp):  int array X's address
#	N:	4($sp): number of words in array X
# returns:
#	&X:	0($sp):  array X is now sorted in increasing order!
# uses:
#	top (2) spots of (given) stack
#	t0
#	t1
#	t2
#	t3
#	t4
#	t5
#	t6
# calls:
#	[this is a leaf function]
#-------------------------------------------------------------------------------
# pseudocode
#	int* t0 = &X;
#	int* t1 = t0 + N - 1;
#	while(t1 > t0){//N == 1 is always sorted already, so we'd be done! :D
#		t2 = t0;
#		do{//bubble up!
#			t3 = *t2;
#			t2++;
#			t4 = *t2;
#			if(t4 < t3){ //swap
#				*(t2-1) = t4;
#				*t2 = t3;
#			} 
#		}while(t2 < t1);//previous loop guarantees this needs to run
#		t1--; //shrink unsorted area; highest number bubbled up already!
#	}
#	return;
#-------------------------------------------------------------------------------
BubSort:
	lw	$t0,0($sp)	#int* t0 = &X;
	lw	$t1,4($sp)	#int* t1 = N;
	addi	$t1,$t1,-word	#t1--;
	add	$t1,$t1,$t0	#t1 += t0; // point at end of array
#//if N == 1 were true, the list is always sorted already, so we'd be done! :D
	b	4f		#while(t1 > t0){
1:	mov	$t2,$t0		#	t2 = t0;
2:	lw	$t3,0($t2)	#	do{ t3 = *t2; //bubble up!
	addi	$t2,$t2,word	#		t2++;
	lw	$t4,0($t2)	#		t4 = *t2;
	ble	$t3,$t4,3f	#		if(t4 < t3){ //swap
	sw	$t4,-word($t2)	#			*(t2-1) = t4;
	sw	$t3,0($t2)	#			*t2 = t3;}
3:	blt	$t2,$t1,2b	#	}while(t2 < t1);//previous loop guarantees this needs to run
	addi	$t1,$t1,-word	#	t1--; //shrink unsorted area; highest number bubbled up already!
4:	ble	$t0,$t1,1b	#}
	jr	$ra		#return;