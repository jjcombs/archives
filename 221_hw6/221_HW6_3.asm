	.data
arrayX:	.ascii	"abcdefghijklmnopqrstuvwxyz"	#L = 26
	.ascii	"ABCDEABCDE';[],.<>-=+~`"	#U = 10 + garbage
	.ascii	"12345:'{}:<>.,/Ʋܣ�"		#D = 5 + garbage
arrayX_end:
#consant that will be our N:
sizeN	= arrayX_end-arrayX
stackSpaceNeeded = 5*word
	.code
main:	addi	$sp,$sp,-stackSpaceNeeded	#room for input/output
	li	$a0,'\f
	syscall	$print_char	#clear screen for testing
	li	$s0,arrayX
	sw	$s0,0($sp)	#store &X
	li	$s1,sizeN
	sw	$s1,4($sp)	#store N

	jal	Scan
#print U (10)
	lb	$a0,8($sp)
	syscall	$print_int
#print L (26)
	li	$a0,'\n
	syscall	$print_char
	lb	$a0,12($sp)
	syscall	$print_int
#print D (5)
	li	$a0,'\n
	syscall	$print_char
	lb	$a0,16($sp)
	syscall	$print_int

	addi	$sp,$sp,stackSpaceNeeded	#clean up stack
	syscall	$exit
################################################################################
# void Scan (&X:0($sp), N:4($sp), U:8($sp), L:12($sp), D:16($sp))
# arguments:
#	&X:0($sp):  byte array X's address
#	 N:4($sp):  number of bytes in array X
#	 U:8($sp):  reserved for return value
#	 L:12($sp): reserved for return value
#	 D:16($sp): reserved for return value
# returns:
#	U:	number of upper-case 	ascii characters
#	L:	number of lower-case 	ascii characters
#	D:	number of decimal-digit ascii characters
# uses:
#	top (5) spots of (given) stack
#	t0
#	t1
#	t2
#	t3
#	t4
#	t5
#	t6
#	t7
# calls:
#	[this is a leaf function]
#-------------------------------------------------------------------------------
# pseudocode
#	char* t0 = &X;
#	char* t1 = t0 + N;
#	unsigned char t2;
#	int t3 = 0; //U
#	int t4 = 0; //L
#	int t5 = 0; //D
#	do{
#		t1--;
#		t2 = *t1;
#		t2 -= 0x30; //t2 is now 0-9 if it was a digit
#		if(t2 < 10)
#			D++;
#		t2 -= 0x11; //t2 is now 0-25 if it was an uppercase letter
#		if(t2 < 26)
#			U++;
#		t2 -= 0x20; //t2 is now 0-25 if it was a lowercase letter
#		if(t2 < 26)
#			L++;
#	}while(t1 > t0);
#	U = t3;
#	L = t4;
#	D = t5;
#	return;
#-------------------------------------------------------------------------------
Scan:	lw	$t0,0($sp)	#load address	&X
	lw	$t1,4($sp)	#load size	N
	add	$t1,$t0,$t1	#t1 points past the end of X
	li	$t3,0		#initialize U
	li	$t4,0		#initialize L
	li	$t5,0		#initialize D
	li	$t6,9		#for if-statement optimization
	li	$t7,25		#for if-statement optimization
1:	addi	$t1,$t1,-1	#t1--;
	lb	$t2,0($t1)	#t2 = *t1;
	addi	$t2,$t2,-0x30	#t2 -= 0x30; //t2 is now 0-9 if it was a digit
	bgtu	$t2,$t6,2f	#if(t2 < 10)
	addi	$t5,$t5,1	#	D++;
2:	addi	$t2,$t2,-0x11	#t2 -= 0x11; //t2 is now 0-25 if it was an uppercase letter
	bgtu	$t2,$t7,3f	#if(t2 < 26)
	addi	$t3,$t3,1	#	U++;
3:	addi	$t2,$t2,-0x20	#t2 -= 0x20; //t2 is now 0-25 if it was a lowercase letter
	bgtu	$t2,$t7,4f	#if(t2 < 26)
	addi	$t4,$t4,1	#	L++;
4:	bne	$t1,$t0,1b	#}while(t1 > t0);
	sw	$t3,8($sp)	#U = t3;
	sw	$t4,12($sp)	#L = t4;
	sw	$t5,16($sp)	#D = t5;
	jr	$ra		#return;