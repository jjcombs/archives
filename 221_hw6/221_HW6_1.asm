	.data
arrayX:	.word -100,0,1,2,3,4,5,6,7,8,9,100
arrayX_end:
#consant that will be our N:
#(don't forget to divide by word size to get number of elements...)
sizeN	= arrayX_end-arrayX/word
	.code
main:	addi	$sp,$sp,-4*word	#room for (&X, N, min, max)
	li	$s0,arrayX
	sw	$s0,0($sp)	#store &X
	li	$s1,sizeN
	sw	$s1,4($sp)	#store N
	jal	MinMax
	li	$a0,'\f
	syscall	$print_char
	lw	$a0,8($sp)	#min
	syscall	$print_int
	li	$a0,'\n
	syscall	$print_char
	lw	$a0,12($sp)	#max
	syscall	$print_int
	addi	$sp,$sp,4*word	#clean up stack
	syscall	$exit

################################################################################
# MinMax (&X:0($sp), N:4($sp), Min:8($sp), Max:12($sp))
# arguments:
#	&X:0($sp):  array X's address
#	 N:4($sp):  lenght of array X
#	 ?:8($sp):  open spot for return value
#	 ?:12($sp): open spot for return value
# returns:
#	Min:8($sp):  will become the smallest value in the array
#	Max:12($sp): will become the largest value in the array
# uses:
#	(given) stack
#	t0
#	t1
#	t2
#	t3
#	t4
# calls:
#	[this is a leaf function]
#-------------------------------------------------------------------------------
# pseudocode
#	int* t0 = &X;
#	int* t1 = t0 + N - 1;
#	int t2 = *(t1-1);
#	int t3 = t2;
#	int t4;
#	do{
#		t1--;
#		t4 = *t1;
#		t2 = (t2 > t4 ? t4 : t2);
#		t3 = (t3 < t4 ? t4 : t3);
#	}while(t1 != t0);
#	Min = t2;
#	Max = t3;
#	return;
#-------------------------------------------------------------------------------
MinMax:	lw	$t0,0($sp)	#load addresses
	lw	$t1,4($sp)	#no error checking! T_T
	addi	$t1,$t1,-1	#pointer arithmatic and not going out of bounds
	sll	$t1,$t1,2
	add	$t1,$t0,$t1	#t1 points to the end of X
	lw	$t2,0($t1)	#initialize our min and max to that value
	mov	$t3,$t2
1:	addi	$t1,$t1,-4	#do {t1--
	lw	$t4,0($t1)	#  t4 = *t1
	bge	$t4,$t2,2f	#  if t2 > t4
	mov	$t2,$t4		#    then t2 = t4
2:	ble	$t4,$t3,3f	#  if t3 < t4
	mov	$t3,$t4		#    then t3 = t4
3:	bne	$t1,$t0,1b	#}while t1 != t0
	sw	$t2,8($sp)
	sw	$t3,12($sp)
	jr	$ra