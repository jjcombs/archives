#include <thread>
#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

vector<int> unsorted_list = {7, 12, 19, 3, 18, 4, 2, 6, 15, 8};
vector<int> sorted_list;

struct sort_params{
    int s;
    int e;
};

struct merge_params{
    int s;
    int m;
    int e;
};

void sort_half(sort_params ends){
    //choosing the standard library sort is a valid choice of sorting algorithm, right?
    sort(unsorted_list.begin()+ends.s, unsorted_list.begin()+ends.e);
}

void muh_merge(merge_params c){
    int i = c.s, j = c.m;
    for(; i < c.m && j < c.e;){
        if(unsorted_list[i] <= unsorted_list[j]){
            sorted_list.push_back(unsorted_list[i]);
            i++;
        }else{
            sorted_list.push_back(unsorted_list[j]);
            j++;
        }
    }
    for(; i < c.m; i++){
        sorted_list.push_back(unsorted_list[i]);
    }
    for(; j < c.e; j++){
        sorted_list.push_back(unsorted_list[j]);
    }
}

int main(){
    //read in unsorted list?
    sort_params a, b;
    a.s = 0;
    b.e = unsorted_list.size();
    b.s = a.e = b.e/2;
    thread t1(sort_half, a);
    thread t2(sort_half, b);
    merge_params c;
    c.s = a.s;
    c.m = a.e;
    c.e = b.e;
    t1.join();
    t2.join();
    thread t3(muh_merge, c);
    t3.join();

    int i = 0;
    cout<<"{";
    for(; i < sorted_list.size() - 1; i++){
        cout<<sorted_list[i]<<", ";
    }
    cout<<sorted_list[i]<<"}"<<endl;

    return 0;
}
