#include <thread>
#include <iostream>
#include <string>
#include <set>

using namespace std;

int sudoku[9][9] = {
      {6, 2, 4, 5, 3, 9, 1, 8, 7}
    , {5, 1, 9, 7, 2, 8, 6, 3, 4}
    , {8, 3, 7, 6, 1, 4, 2, 9, 5}
    , {1, 4, 3, 8, 6, 5, 7, 2, 9}
    , {9, 5, 8, 2, 4, 7, 3, 6, 1}
    , {7, 6, 2, 3, 9, 1, 4, 5, 8}
    , {3, 7, 1, 9, 5, 6, 8, 4, 2}
    , {4, 9, 6, 1, 8, 2, 5, 7, 3}
    , {2, 8, 5, 4, 7, 3, 9, 1, 6}
    };

bool answer_is_correct = true;
const set<int> test_set = {1,2,3,4,5,6,7,8,9};

struct box{
    int x;
    int y;
};

void check_box(box abox){
    int x_end = (abox.x+1)*3;
    int y_end = (abox.y+1)*3;
    set<int> box_set;
    for(int i = x_end - 3; i < x_end; i++){
        for(int j = y_end - 3; j < y_end; j++){
            box_set.insert(sudoku[i][j]);
        }
    }
    if(box_set.size() < 9)
        answer_is_correct = false;
}

void check_col(int x){
    set<int> col_set;
    for(int i = 0; i < 9; i++){
        col_set.insert(sudoku[x][i]);
    }
    if(col_set.size() < 9)
        answer_is_correct = false;
}

void check_row(int y){
    set<int> row_set;
    for(int i = 0; i < 9; i++){
        row_set.insert(sudoku[i][y]);
    }
    if(row_set.size() < 9)
        answer_is_correct = false;
}

int main(){
    //read in sudoku?


    thread *boxes[3][3], *cols[9], *rows[9];

    for(int i = 0; i < 9; i++){
        cols[i] = new thread(check_col, i);
        rows[i] = new thread(check_row, i);
    }
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            static box abox;
            abox.x = i;
            abox.y = j;
            boxes[i][j] = new thread(check_box, abox);
        }
    }



    for(int i = 0; i < 9; i++){
        cols[i]->join();
        rows[i]->join();
        delete cols[i];
        delete rows[i];
    }
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            boxes[i][j]->join();
            delete boxes[i][j];
        }
    }
    if(answer_is_correct)
        cout<<"the given sudoku board is a valid solution.\n";
    else
        cout<<"the given sudoku board is NOT a valid solution.\n";

    return 0;
}
