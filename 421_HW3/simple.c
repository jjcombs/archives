#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/list.h>
#include <linux/slab.h>

struct birthday {
	int day;
	int month;
	int year;
	struct list_head list;
};


static LIST_HEAD(birthday_list);
static struct birthday *ptr, *next;
static int i;

/* This function is called when the module is loaded. */
int simple_init(void)
{
	printk(KERN_INFO "Loading module...\n");

	for(i = 0; i < 5; i = i + 1){
		ptr = kmalloc(sizeof(*ptr), GFP_KERNEL);
		ptr->day = 22+i;
		ptr->month = 3+i;
		ptr->year = 1987+i;
		INIT_LIST_HEAD(&ptr->list);

		list_add_tail(&ptr->list, &birthday_list);
	}

	list_for_each_entry(ptr, &birthday_list, list){
		printk("birth date (year-month-day): %d-0%d-%d", ptr->year, ptr->month, ptr->day);
	}

	printk(KERN_INFO "simple.ko module loaded!\n");

	return 0;
}

/* This function is called when the module is removed. */
void simple_exit(void) {
	printk(KERN_INFO "Removing module...\n");

	
	list_for_each_entry_safe(ptr, next, &birthday_list, list){
		list_del(&ptr->list);
		kfree(ptr);
	}
	printk(KERN_INFO "simple.ko module removed!\n");

}

/* Macros for registering module entry and exit points. */
module_init( simple_init );
module_exit( simple_exit );

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Simple Module");
MODULE_AUTHOR("JJC");
