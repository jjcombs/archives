#Jonathan Combs
	.code
	.globl 	main
main:	la	$a0,calc_start
	syscall	$print_string
	syscall	$exit
	.data
calc_start:	#includes the screen clear ! yay!
	.ascii	"\f\tDesign goal for Calculator Project - Fall 2018\n"
	.ascii 	"旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�\n"
	.ascii 	"�  旼컴컴컴컴컴컴컴컴커                 旼컴컴컴컴컴컴컴컴컴컴컴컴컴커  �\n"
	.ascii	"�  놨 Dec o Hex o Bin �                 �    -1.7976931348623157e+308�  �\n"
	.ascii	"�  읕컴컴컴컴컴컴컴컴켸                 읕컴컴컴컴컴컴컴컴컴컴컴컴컴켸  �\n"
	.ascii 	"�  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴왯컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴�  �\n"
	.ascii	"�  놨 Degrees o Radians o Grads  납 D  볐 E  볐 F  볐 M- 볐 M+ 볐 <= �  �\n"
	.ascii	"�  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴朦袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴�  �\n"
	.ascii	"�        旼컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴�  �\n"
	.ascii	"�        � Inv 볐 ln 볐 (  볐  ) 볐 A  볐 B  볐 C  볐 �  볐 �  볐 MS �  �\n"
	.ascii	"�        突袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴�  �\n"
	.ascii	"�  旼컴캠旼컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴�  �\n"
	.ascii	"�  � Int볐 sinh볐 sin볐 x� 볐 n! 볐 7  볐 8  볐 9  볐 /  볐 %  볐 MR �  �\n"
	.ascii	"�  突袴暠突袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴�  �\n"
	.ascii	"�  旼컴캠旼컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴�  �\n"
	.ascii	"�  � dms볐 cosh볐 cos볐 x� 볐 曉x볐 4  볐 5  볐 6  볐 *  볐 1/x볐 MC �  �\n"
	.ascii	"�  突袴暠突袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴�  �\n"
	.ascii	"�  旼컴캠旼컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴�  �\n"
	.ascii	"�  � �  볐 tanh볐 tan볐 x^3볐 3�x볐 1  볐 2  볐 3  볐 -  볐    볐 CE �  �\n"
	.ascii	"�  突袴暠突袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴솎袴袴설 賽 붊袴袴�  �\n"
	.ascii	"�        旼컴컴뢰컴컴뢰컴컴뢰컴컴뢰컴컴컴컴컴뢰컴컴뢰컴컴럼 賽 붚컴컴�  �\n"
	.ascii	"�        � Exp 볐 Mod볐 log볐 10煥�    0     볐 6  볐 +  볐    볐 C  �  �\n"
	.ascii	"�        突袴袴솎袴袴솎袴袴솎袴袴솎袴袴袴袴袴솎袴袴솎袴袴솎袴袴솎袴袴�  �\n"
	.asciiz	"읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�\n"