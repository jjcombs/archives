	.data
box:	.struct
tl:	.byte	0
tm:	.byte	0
tr:	.byte	0
ml:	.byte	0
mm:	.byte	0
mr:	.byte	0
bl:	.byte	0
bm:	.byte	0
br:	.byte	0
	.data			#marks end of struct
upbutton.box:
	.ascii	"�ķ�"
	.byte	0xff		#center is hollow
	.ascii	"��ͼ"
#	.ascii	"�ķ"
#	.ascii	"� �"
#	.ascii	"�ͼ"
downbutton.box:
	.ascii	"�͸�"
	.byte	0xff		#center is hollow
	.ascii	"����"
#	.ascii	"�͸"
#	.ascii	"� �"
#	.ascii	"���"
hthin.box:
	.ascii	"�Ŀ�"
	.byte	0xff		#center is hollow
	.ascii	"����"
thin.box:
	.ascii	"�Ŀ"
	.ascii	"� �"
	.ascii	"���"
hdouble.box:
	.ascii	"�ͻ�"
	.byte	0xff		#center is hollow
	.ascii	"��ͼ"
double.box:
	.ascii	"�ͻ"
	.ascii	"� �"
	.ascii	"�ͼ"
hthick.box:
	.ascii	"����"
	.byte	0xff		#center is hollow
	.ascii	"����"
thick.box:
	.ascii	"���"
	.ascii	"� �"
	.ascii	"���"
#etc
	.code
###########################################################################
#needs 0xff in the center spot in order to draw only edges...
box.draw:			#function o' doooooom and drawing
#void box::draw(int x:a0, int y:a1, int w:a2, int h:a3, this *box:s0)
	mov	$t0,$a0		#save for later use
	syscall	$xy
	lb	$a0,box.tl($s0)	#only one top left corner
	syscall	$print_char
	addi	$t2,$a2,-2	#no error checking, box size it at least 3x3
	lb	$a0,box.tm($s0)	#could be lots of top middle parts
1:	syscall	$print_char	#do{cout << box.tm;
	addi	$t2,$t2,-1	#	i--;
	bgtz	$t2,1b		#}while(i > w - 2);
	lb	$a0,box.tr($s0)	#only one upper right corner
	syscall	$print_char
	addi	$t3,$a3,-2	#still no error checking
2:	mov	$a0,$t0		#x for next row reset!
	addi	$a1,$a1,1	#wont need to reuse y :D
	syscall	$xy		#next row
	lb	$a0,box.ml($s0) #one middle left edge per row
	syscall	$print_char
	addi	$t2,$a2,-2	#load width again!
	lb	$a0,box.mm($s0)
1:	syscall	$print_char	#print out all the middle!
	addi	$t2,$t2,-1
	bgtz	$t2,1b
	lb	$a0,box.mr($s0)	#print out the right edge
	syscall	$print_char
	addi	$t3,$t3,-1	#decrement height counter
	bgtz	$t3,2b
	mov	$a0,$t0		#finally bottom row!
	addi	$a1,$a1,1	#a1 now points to bottom!
	syscall	$xy
	lb	$a0,box.bl($s0)	#only 1 lower left corner
	syscall	$print_char
	addi	$t2,$a2,-2	#no error checking
	lb	$a0,box.bm($s0) #lots of bottom middle pieces
1:	syscall	$print_char
	addi	$t2,$t2,-1
	bgtz	$t2,1b
	lb	$a0,box.br($s0)	#only one lower right
	syscall	$print_char
	jr	$ra		#we're done!
###########################################################################
	.code
	.globl	main
main:	li	$a0,'\f
	syscall	$print_char
	la	$s0,thin.box
	li	$a0,5
	li	$a1,5
	li	$a2,5
	li	$a3,5
	jal	box.draw
	li	$a0,4
	li	$a1,4
	li	$a2,5
	li	$a3,5
	jal	box.draw
	syscall	$exit