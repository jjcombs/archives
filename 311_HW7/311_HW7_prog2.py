#By Jonathan Combs
#Python problem for HW7

class student_information:
	def __init__(self, name, netid, gpa):
		self.name = name
		self.netid = netid
		self.gpa = gpa
	def __str__(self):
		output = "Student name: "
		output += self.name
		output += "\nStudent GPA: "
		output += str(self.gpa)
		return output

x = []
names = ["Ichi", "Ni", "San", "Shi", "Go"]
gpas = [2, 3.5, 9001, 3, 4]
for i in range(0,5):
	x.append(student_information(names[i],i,gpas[i]))

def findByNETID(netid):
	for a in x:
		if(a.netid == netid):
			print(a)

findByNETID(2)
findByNETID(4)
findByNETID(1)
