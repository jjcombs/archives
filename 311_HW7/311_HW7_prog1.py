#By Jonathan Combs
#Python problem for HW7

class retail_item:
	def __init__(self, description, units, price):
		self.description = description
		self.unitsOnHand = units
		self.price = price
	def getPriceWithTax(self):
		total = self.price * 1.08
		return total
	def removeItem(self):
		self.unitsOnHand -= 1
	def __str__(self):
		output = "Our "
		output += self.description
		output += " cost $"
		output += str(self.price)
		output += " each, and we have "
		output += str(self.unitsOnHand)
		output += " left in stock."
		return output

def handle_purchase(retail_item, count):
	if(count > retail_item.unitsOnHand):
		count = retail_item.unitsOnHand
		print("Sorry, we only have", count, retail_item.description + ", but you can have all of them.")
	for i in range(0,count):
		retail_item.removeItem
	bill = retail_item.getPriceWithTax() * count
	bill = round(bill,2)
	print()
	print("Your total (for",count,retail_item.description + ") is $" + str(bill))
print()
s = retail_item("spoons", 10, 10.42)
k = retail_item("knives", 20, 13.37)
f = retail_item("forks", 15, 101.01)

print(s)
print(k)
print(f)
print()
print("What would you like to buy?")
choice = str(input("(f for Forks, k for Knives, s for Spoons): "))
if("f" != choice[0] and "F" != choice[0] and
		"k" != choice[0] and "K" != choice[0] and
		"s" != choice[0] and "S" != choice[0]):
	print(choice, "is an invalid choice, bye!")
else:
	count = int(input("How many? "))
if("f" == choice[0] or "F" == choice[0]):
	handle_purchase(f, count)	
elif("k" == choice[0] or "K" == choice[0]):
	handle_purchase(k, count)	
elif("s" == choice[0] or "S" == choice[0]):
	handle_purchase(s, count)	
