#include <iostream>
#include <string>

using namespace std;


void isPalindrome(const string &a_string){
    if(a_string.size() < 2){
        //return;
    } else {
        // if the last character equals the first (ignoring capitalization)
        if(tolower(a_string[0]) == tolower(a_string[a_string.size()-1])){
            // then check the next two inner characters
            isPalindrome(a_string.substr(1,a_string.size()-2));
        } else {
            // otherwise, it isn't a palindrome!
            cout << " not";
        }
    }
}

int main(){
    string testable_string;
    char c;
    do{
        testable_string = "";
        cout << "Enter a string to test (type q to quit): ";
        //getline hates me for some reason, so I'm doing something weird:
        cin.get(c);
        do{
            testable_string += c;
            cin.get(c);// this comes second to avoid the \n from hitting Enter
        }while(c != '\n');
        cout << "The string '" << testable_string << "' is";
        isPalindrome(testable_string);// prints " not" if not a palindrome
        cout << " a palindrome\n";
    }while(testable_string != "q");
    //yes, it will still say that q is a palindrome before saying bye xD
    cout << "\n\nBye!\n";
    return 0;
}
