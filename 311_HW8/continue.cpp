

int main(){
	bool prime_tracker[100] = {true};
	int i, j;
	//2 is the smallest prime number:
	prime_tracker[0] = false;
	prime_tracker[1] = false;
	for(i = 2; i < 100; i++){
		if(prime_tracker[i] == false){
			continue;
			//the continue is needed to skip the numbers that aren't prime
		}

		//set all the multiples of this prime, i, to false:
		for(j = i*2; j < 100; j += i){
			prime_tracker[j] = false;
		}
	}
	return 0;
}
