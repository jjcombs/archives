#ifndef MUH_BUCKET_H
#define MUH_BUCKET_H

//I hope you enjoy my silly testing of how templates work past this first class
// declaration...
template <class T>
class muh_bucket {
    public:
        muh_bucket();
        virtual ~muh_bucket();
        bool isEmpty();     //check if the bucket is empty
        T pop();            //take something off the top and return it
        void push(T item);  //add something into the bucket

    protected:

    private:
        struct Node{
            T item;
            Node *next;
        };
        Node *top;
};

template <class muh>
muh_bucket<muh>::muh_bucket() {// need a null pointer at first
    top = nullptr;
}

template <class bucket>
muh_bucket<bucket>::~muh_bucket() {// clear whole linked list
    Node *temp;
    while(top){
        temp = top->next;
        delete top;
        top = temp;
    }
}

template <class Boop>
bool muh_bucket<Boop>::isEmpty() {// empty is easy to check...
    return (top == nullptr);
}

template <class T>
T muh_bucket<T>::pop() {// goes the weasel
    T temp = top->item;
    Node *old_node = top;
    top = top->next;
    delete old_node;
    return temp;
}

template <class U>
void muh_bucket<U>::push(U item) {
    Node *new_node = new Node;
    new_node->next = top;
    new_node->item = item;
    top = new_node;
}

#endif // MUH_BUCKET_H
