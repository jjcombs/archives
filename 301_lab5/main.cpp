#include "muh_bucket.h"
#include <fstream>
#include <string>
#include <iostream>
#include <sstream>

using namespace std;

int main(int argc, char **argv){
	ifstream in_file;
	string file_name;
//I should probably move these two if statements into a function...
	if(argc > 1){//always at least 1 (the program itself is an argument)
		//argv[0] is the call to this program itself
		//argv[1] is the first thing after the space
		file_name = argv[1];
		in_file.open(argv[1]);
		if(!in_file.is_open()){
			cout << "Sorry, command line argument invalid: \"";
			cout << argv[1] << "\" not found! Please try again...\n\n";
	    	}
	}
	if(!in_file.is_open()){
		for(int i = 0; i < 10; i++) {// you get 10 tries, that's it
			cout << "Please enter the name of the file you'd like reversed\n";
			cout << "(including the directory, you might need a './'\n";
			cout << "in front to refer to the current directory):\n";
			cin >> file_name;
			in_file.open(file_name.c_str());
			if(in_file.is_open()){
				break;
			} else if(i < 9){// will the user get it right if I say please?
				cout << '"' << file_name;// ...or pretty please?
				cout << "\" not found, please try again...\n\n";
			} else {// if we get here, we're so done at this point...
				cout << "\n\nI give up! Bye!\n\n";
			}
		}
	}
// all that to open a file... or give up on that xD
// why didn't I decide to just crash this program instead of testing so much?
	if(in_file.is_open()){
		muh_bucket<char> char_bucket = muh_bucket<char>();
		char c;
		while(in_file.get(c)){
			char_bucket.push(c);
		}
		in_file.close();
		file_name = "reversed_" + file_name;
		ofstream reversed_file = ofstream(file_name.c_str());
		while(!char_bucket.isEmpty()){
			c = char_bucket.pop();
			reversed_file << c;
		}
		reversed_file.close();
		cout << "New file created/nuked: \"" << file_name << "\"\n";
	}
	return 0;
}
