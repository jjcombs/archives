typedef int ItemType;
template<class ItemType>
vector<ItemType> MergeSort(vector<ItemType> list){
	// check that input list is larger than size 1 or just return the list
	int size = list.size();
	if(size < 2)
		return list;

	// split the one list into two lists for sorting
	vector<ItemType> left;
	vector<ItemType> right;
	int i, j;
	for(i = 0; i < size/2; i++)
		left.push_back(list.at(i));
	for(; i < size; i++)
		right.push_back(list.at(i));
	left = MergeSort(left); //sort
	right = MergeSort(right); //sort

	// merge the two pieces in order
	list.clear();
	j = 0;
	i = 0;
	while(i < left.size() && j < right.size()){
		if(left.at(i) < right.at(j)){
			list.push_back(left.at(i));
			i++;
		}else{
			list.push_back(right.at(j));
			j++;
		}
	}
	//if there was extra in the "left" half, put it on top
	while(i < left.size()){
		list.push_back(left.at(i));
		i++;
	}
	//if there was extra in the "right" half, put it on top
	while(j < right.size()){
		list.push_back(right.at(j));
		j++;
	}
		
	return list;// return the sorted list
}

template<class ItemType>
void MergeSort(ItemType values[],int blah, int numValues){
	numValues++;//fix SIZE-1
	vector<ItemType> list(numValues);
	//pull from values array into vector
	for(int i = 0; i < numValues; i++)
		list[i] = values[i];
	list = MergeSort(list); //sort
	//put back into values array
	for(int i = 0; i < numValues; i++)
		values[i] = list[i];
}



