typedef int ItemType;
template<class ItemType>
void InsertionSort(ItemType values[], int numValues){
	numValues++;//fix SIZE-1
	// need a variable to keep track of the sorted portion boundry: i
	// we'll also need to shuffle up the numbers as we insert down into the
	//  bottom half, so let's do something bubble-down-ish
	for(int i = 1; i < numValues; i++)
		for(int j = i; j > 0 && values[j-1] > values[j]; j--)
			Swap(values[j-1], values[j]);
}




