// heap stuff shamelessly adapted from lab 9!
//  yay code reuse!

typedef int ItemType;
template<class ItemType>
void ReheapDown(ItemType elements[], int root, int bottom)
// Post: Heap property is restored.
{
//we just swapped the first and last element
//now we need to swap the root number down to an appropriate node

    //first, we need to check if our currently targeted node has a larger
    // value than both of its children, we can compare with the larger one
    int larger_child = root * 2 + 1;//assume left child is larger
    if(larger_child > bottom)
        return;//no children! we're already a leaf node! DONE!

    if(larger_child < bottom &&//make sure that there exists a right child
        elements[larger_child] < elements[larger_child + 1])//is it bigger?
        larger_child++;//pick right child, if it's larger
    //yes it's only one line after the condition, it's a big condition...

    if(elements[root] >= elements[larger_child])
        return;//we're larger than or equal, yay! DONE!
    //else unneeded
    //if we haven't returned from being done, we need to swap the parent
    // with its larger child
    Swap(elements[root],elements[larger_child]);
    //and keep going until we stop early due to a base case:
    ReheapDown(elements, larger_child, bottom);
}

template<class ItemType>
void ReheapUp(ItemType elements[], int root, int bottom)
// Post: Heap property is restored.
{
//we just appended an element to the list
//now we need to move the newly added item to an appropriate place in the heap

    //first, we need to check if our current node's parent is greater
    int parent_of_bottom = (bottom - 1) / 2; //-1 will remove the +1 for
    // the left child and cause a +2 from the right child to get truncated away
    if(bottom == root || // if we're checking the root, we're done
       elements[parent_of_bottom] > elements[bottom])
        return; // we're done!
    //else
    //if we didn't return, we need to swap
    Swap(elements[parent_of_bottom], elements[bottom]);
    //and keep going
    ReheapUp(elements, root, parent_of_bottom);
}

template<class ItemType>
void HeapSort(ItemType values[], int numValues){
    // first we build the heap
    //  a single element by itself is in order: i = 1
    for(int i = 1; i < numValues; i++) 
        ReheapUp(values, 0, i);
    // then we swap them in order of size and fix heap,
    //  we don't need to swap on i = 0
    for(int i = numValues - 1; i > 0;){
        Swap(values[0],values[i]);
        i--; // the i-th value is no longer in the heap
            //   might as well decrement in the middle...
        ReheapDown(values, 0, i);
    }
}




