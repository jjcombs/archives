typedef int ItemType;
template<class ItemType>
void SelectionSort(ItemType values[], int numValues){
	numValues++;//fix SIZE-1
	// need a variable to keep track of the sorted portion boundry and the
	// smallest value
	int small_idx;
	for(int boundry = 0; boundry < numValues - 1; boundry++){
		// find smallest value in unsorted section
		small_idx = boundry;
		for(int i = boundry + 1; i < numValues; i++){
			if(values[i] < values[small_idx])
				small_idx = i;
		}
		// swap smallest value into the sorted portion
		Swap(values[boundry], values[small_idx]);
	}
}

