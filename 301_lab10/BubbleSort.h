typedef int ItemType;
template<class ItemType>
void BubbleSort(ItemType values[], int numValues){
	numValues++; //because we're given SIZE-1
	// need a variable to keep track of the sorted portion boundry
	// we'll use i!
	for(int i = numValues - 1; i > 0; i--)//no initial subtraction: we were given SIZE-1
		for(int j = 0; j < i; j++)
			if(values[j] > values[j+1])
				Swap(values[j], values[j+1]);// use Swap function provided...
}




