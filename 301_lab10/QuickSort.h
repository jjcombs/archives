typedef int ItemType;
template<class ItemType>
//implement Lomuto's partition scheme more-or-less as it appears on wikipedia
void QuickSort(ItemType values[],int min_idx, int max_idx){
	// check base case (if end - begin < 1 gives either a 0 or 1 sized list,
	if(max_idx <= min_idx)
		return;	// then it's already sorted)
	// need to pick a pivot (always start with lowest index,
	//  'cause sorted input shouldn't happen)
	// need index trackers, i to track new pivot spot and j to track swaps
	int i = min_idx;
	for(int j = min_idx; j < max_idx; j++){
		if(values[j] < values[max_idx]){
			if(i != j)
				Swap(values[i], values[j]);
			i++;
		}
	}
	// swap pivot into position
	Swap(values[max_idx], values[i]);

	QuickSort(values, min_idx, i - 1); // recurse      !
	QuickSort(values, i + 1, max_idx); //   curse again!
}

