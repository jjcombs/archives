// This program is a sorting algorithm tester.

// Input are sorting algorithm names plus Refresh for resetting the
//   values to be sorted to their previous values and Reinitialize for
//   creating a new set of values.
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <algorithm> 
#include <chrono>
#include <vector>
const int SIZE = 1000;

using namespace std;
using namespace std::chrono; 

//for the time collection array!
enum {
	SELECTION,
	INSERTION,
	MERGE,
	QUICK1,
	QUICK2,
	BUBBLE,
	SHORTBUBBLE,
	HEAP,
	NUMBER_OF_SORTS
};

typedef int ItemType;

// SIZE should be a multiple of 10.

void Print(ofstream&, int[]);	// Prints array
void InitValues(int[]);			// Creates random array 
void CopyValues(int[], int[]);	// Makes a copy of random array

void Swap(ItemType& item1, ItemType& item2);

#include "SelectionSort.h"
#include "InsertionSort.h"
#include "MergeSort.h"
#include "QuickSort.h"
#include "QuickSort2.h"
#include "BubbleSort.h"
#include "ShortBubble.h"
#include "HeapSort.h"

int main()
{

  ifstream inFile;       // file containing operations
  ofstream outFile;      // file containing output
  string inFileName;     // input file external name
  string outFileName;    // output file external name
  string outputLabel;     
  string command;        // operation to be executed
  ItemType values[SIZE];
  int numCommands;
  ItemType copyValues[SIZE];
  // Get type of timepoint by using the clock...
  auto start = high_resolution_clock::now();
  auto stop = high_resolution_clock::now();
  auto duration = duration_cast<microseconds>(stop - start);
  // To store the times and the names of the sorts:
  vector<long long int> times[NUMBER_OF_SORTS];
  string names[NUMBER_OF_SORTS];
  names[SELECTION]		= string("selection");
  names[INSERTION]		= string("insertion");
  names[MERGE]			= string("merge");
  names[QUICK1]			= string("(recursing) quick");
  names[QUICK2]			= string("(looping) quick");
  names[BUBBLE]			= string("bubble");
  names[SHORTBUBBLE]	= string("short bubble");
  names[HEAP]			= string("heap");
 
  // Prompt for file names, read file names, and prepare files
  cout << "Enter name of input command file; press return." << endl;
  cin  >> inFileName;
  inFile.open(inFileName.c_str());

  cout << "Enter name of output file; press return." << endl;
  cin  >> outFileName;
  outFile.open(outFileName.c_str());

  cout << "Enter name of test run; press return." << endl;
  cin  >> outputLabel;
  outFile << outputLabel << endl;

  inFile >> command;
  InitValues(values);
  CopyValues(values, copyValues);
  outFile << "Initial values" << endl;
  Print(outFile, values);

  numCommands = 0;
  while (command != "Quit")
  { 
    if (command == "SelectionSort")
    {
      start = high_resolution_clock::now();
      SelectionSort(values, SIZE-1);
      stop = high_resolution_clock::now();
      duration = duration_cast<microseconds>(stop - start);
      times[SELECTION].push_back(duration.count());
      outFile << "Results from SelectionSort: " << endl;
      Print(outFile, values);
      outFile << endl;
    }
    else if (command == "BubbleSort")
    {
      start = high_resolution_clock::now();
      BubbleSort(values, SIZE-1);
      stop = high_resolution_clock::now();
      duration = duration_cast<microseconds>(stop - start);
      times[BUBBLE].push_back(duration.count());
      outFile << "Results from BubbleSort: " << endl;
      Print(outFile, values);
      outFile << endl;
    }
    else if (command == "ShortBubble")
    {
      start = high_resolution_clock::now();
      ShortBubble(values, SIZE-1);
      stop = high_resolution_clock::now();
      duration = duration_cast<microseconds>(stop - start);
      times[SHORTBUBBLE].push_back(duration.count());
      outFile << "Results from ShortBubble: " << endl;
      Print(outFile, values);
      outFile << endl;
    } 
    else if (command == "MergeSort")
    {
      start = high_resolution_clock::now();
      MergeSort(values, 0,  SIZE-1);
      stop = high_resolution_clock::now();
      duration = duration_cast<microseconds>(stop - start);
      times[MERGE].push_back(duration.count());
      outFile << "Results from MergeSort: " << endl;
      Print(outFile, values);
      outFile << endl;
    }  
    else if (command == "QuickSort")
    {
      start = high_resolution_clock::now();
      QuickSort(values,  0, SIZE-1);
      stop = high_resolution_clock::now();
      duration = duration_cast<microseconds>(stop - start);
      times[QUICK1].push_back(duration.count());
      outFile << "Results from QuickSort: " << endl;
      Print(outFile, values);
      outFile << endl;
    }
    else if (command == "QuickSort2") 
    {
      start = high_resolution_clock::now();
      QuickSort2(values, 0,  SIZE-1);
      stop = high_resolution_clock::now();
      duration = duration_cast<microseconds>(stop - start);
      times[QUICK2].push_back(duration.count());
      outFile << "Results from QuickSort2: " << endl;
      Print(outFile, values);
      outFile << endl;
    }
     else if (command == "InsertionSort")
    {
      start = high_resolution_clock::now();
      InsertionSort(values,  SIZE-1);
      stop = high_resolution_clock::now();
      duration = duration_cast<microseconds>(stop - start);
      times[INSERTION].push_back(duration.count());
      outFile << "Results from InsertionSort: " << endl;
      Print(outFile, values);
      outFile << endl;
    }
    else if (command == "HeapSort")
    {
      start = high_resolution_clock::now();
      HeapSort(values, SIZE);
      stop = high_resolution_clock::now();
      duration = duration_cast<microseconds>(stop - start);
      times[HEAP].push_back(duration.count());
      outFile << "Results from HeapSort: " << endl;
      Print(outFile, values);
      outFile << endl;
    }  
    else if (command ==  "Refresh")
      CopyValues(copyValues, values);
    else if (command == "ReInitialize")
    {
      InitValues(values);
      CopyValues(values, copyValues);
      outFile << endl << "Initial Values: " << endl;
      Print(outFile, values);
    }  
    else
      outFile << "Input not recognized." << endl;  
      
    numCommands++;
    cout <<  " Command " << command << " completed." 
         << endl;
    inFile >> command;

  }
 
  cout << "Testing completed."  << endl;
  inFile.close();
  outFile.close();

  cout << "Test run times (in microseconds):\n";
  bool there_are_times_to_print;
  int j;
  for(int i = 0; i < NUMBER_OF_SORTS; i++){
    there_are_times_to_print = times[i].size() > 0;
    cout <<"For the " << names[i] << " sort, the time(s) were: ";
    if(there_are_times_to_print){
        j = times[i].size() - 1; // count down to avoid multiple function calls
        cout << times[i][j];
        for(j--; j >= 0; j--){
          cout << ", " << times[i][j];
        }
    }else{
      cout << "not tested?!";
    }
    cout << endl;
  }
  return 0;
}

void InitValues(int values[])
// initializes the values array with random integers from 0 to 99
{
  for (int index = 0; index < SIZE; index++)
    values[index] = (std::rand() % 1000);  
}

void Print(ofstream& outFile, int values[])
{
  using namespace std;
  for (int count = 0; count < SIZE; count= count+10)
    outFile << values[count]  << ", " << values[count+1] << ", " << values[count+2] << ", " 
    << values[count+3] << ", " << values[count+4] << ", " << values[count+5] << ", " 
    << values[count+6] << ", " << values[count + 7] << ", " << values[count + 8] 
    << ", " << values[count+9] << endl << endl;
    
}      

void Swap(ItemType& item1, ItemType& item2)
// Post: Contents of item1 and item2 have been swapped.
{
  ItemType tempItem;

  tempItem = item1;
  item1 = item2;
  item2 = tempItem;
}

void CopyValues(int inData[], int outData[])
{
  for (int count = 0; count < SIZE; count++)
    outData[count] = inData[count];
}    
