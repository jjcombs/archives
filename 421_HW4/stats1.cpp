#include <thread>
#include <iostream>

using namespace std;

int mean;
int maximum;
int minimum;
int SIZE;
int *data;

void findMean(){
	mean = 0.0;
	for(int i = 0; i < SIZE; i++){
		mean += data[i];
	}
	mean /= SIZE;
}

void findMax(){
	maximum = data[0];
	for(int i = 1; i < SIZE; i++)
		if(maximum < data[i])
			maximum = data[i];
}

void findMin(){
	minimum = data[0];
	for(int i = 1; i < SIZE; i++)
		if(minimum > data[i])
			minimum = data[i];
}



int main(int argc, char** argv){
	if(argc < 2){
		cout<<"no input array!"<<endl;
	}else{
		SIZE = argc-1;
		data = new int[SIZE];
		for(int i = 1, j = 0; i < argc; i++, j++){
			data[j] = stoi(argv[i]);
		}

		thread t1(findMean);
		thread t2(findMin);
		thread t3(findMax);
		t1.join();
		cout<<"The average value is "<<mean<<endl;
		t2.join();
		cout<<"The minimum value is "<<minimum<<endl;
		t3.join();
		cout<<"The maximum value is "<<maximum<<endl;
		delete[] data;
	}
	return 0;
}
