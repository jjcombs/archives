#include <thread>
#include <iostream>

using namespace std;

int SIZE;
bool *sieve;

void primes(){
	for(int i = 2; i <= SIZE; i++){
		if(sieve[i]){
			cout<<i<<endl;
			for(int j = i+i; j <= SIZE; j+=i){
				sieve[j] = false;
			}
		}
	}
}


int main(int argc, char** argv){
	if(argc < 2){
		cout<<"Please input a number!"<<endl;
	}else{
		SIZE = stoi(argv[1]);
		sieve = new bool[SIZE+1];
		sieve[0] = sieve[1] = false;
		for(int i = 2; i <= SIZE; i++){
			sieve[i] = true;
		}
		cout<<"Primes less than or equal to "<<SIZE<<':'<<endl;
		thread p(primes);
		p.join();
		delete[] sieve;
	}
	return 0;
}
