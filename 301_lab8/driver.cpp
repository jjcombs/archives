#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>
#include <fstream>
//using the standard library queue, embracing muh laziness
#include <queue>
#include "BinarySortTree.h"

using namespace std;


//random int-list generator:
void RILG(string const &file_name){//random int list generator (OF DOOM)
    static const int some_large_number = 1000;
    ofstream some_new_file;
	//open/nuke the file! Nuke it from orbit, it's the only way to be sure.
    some_new_file.open(file_name.c_str());
	//big loop for lots of numbers, need more than 99 elements
    for(int i = 0; i < some_large_number; i++){
		//generate random int, shove it into file, shove newline into file
        some_new_file << rand() << endl;
    }
	//close file, 'cause why not?
	some_new_file.close();
}


int main(){
	srand(time(NULL));//seed the random number god, lol
	//hardcode file name, 'cause why not? :D
	const string file_name = "./plz_nuke_me.txt";
	//run random file generator on it! ftw!
	RILG(file_name);


	//first make sure we open the file correctly
	ifstream number_file;
	number_file.open(file_name.c_str());
	if(number_file.is_open()){
		int a_num;
		BinarySortTree<int> bst;
		//read in each number, cout as well
		while(number_file >> a_num){
			cout << a_num << endl;
			//insert into BinarySortTree!
			bst.insert(a_num);
		}


		cout << "\n\n"; //separate original list from sorted one

		//convert (queue everything in order, secretly, into the secret queue)
		std::queue<int> a_queue;
		bst.shove_BST_into_Q(a_queue);

		//dequeue each item, checking that each is greater than the previous
		// one, and printing each item in turn
		int last_num = a_queue.front();
		int this_num;
		while(!a_queue.empty()){
			this_num = a_queue.front();
			a_queue.pop();
			if(this_num < last_num){
				//somehow it wasn't sorted...lol
				cout << "\nOOPS!\nUm...\n...\nBYE!\n\n";
				return 9001;//my screwup level would be... OVER 9000!!!!!!!!!!!
			}
			cout << this_num << endl;
			last_num = this_num;
		}
	}else{
		cout << "How did we mess up opening the file we just made?\n";
		return 42;//the answer to even this question ^
	}
    return 0;
}
