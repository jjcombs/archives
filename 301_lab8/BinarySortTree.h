#ifndef BINARYSORTTREE_H
#define BINARYSORTTREE_H

//using the standard library queue, embracing muh laziness
#include <queue>

//template BST
//template + laziness = everything in the header file! ENJOY!
template <class T>
class BinarySortTree {
private:
	//single node in the BST
	struct node{//standard BST node with boring non-punny name :P
		T datum;    // this node's datum
		node *left; // this node's left child
		node *right;// this node's right child
	};
	node *root;
public:
	BinarySortTree();  //make an empty tree
	~BinarySortTree(); //destroy the tree! D:<
	void insert(T);         //grow the tree! in order!
	void shove_BST_into_Q(std::queue<T>&); //run after the BST is populated
	//should probably implement more methods; I'll do that when I need 'em...
private://here's a thing I realized would be helpful:
	void nukeBranch(node*); //from orbit, it's the only way to be sure
	void enqueue_in_order(node*, std::queue<T>&);//recursive function to enqueue
                                            // everything in order
};

template <class T>
BinarySortTree<T>::BinarySortTree(){//make an empty tree
    root = nullptr;
}

template <class T>
BinarySortTree<T>::~BinarySortTree(){//destroy the tree! D:<
    nukeBranch(root);
    //no need to set 'root = nullptr', the whole class is going poof
}

template <class T>
void BinarySortTree<T>::insert(T new_datum){//grow the tree! in order!
    //make the new node for our next number
    node *new_node;
    new_node = new node;
    new_node->datum = new_datum;
    new_node->left  = nullptr;
    new_node->right = nullptr;
    if(root){//we need to find the place
        node *temp;
        //begin at the beginning!
        temp = root;
        bool not_found = true;
        //walk down the tree till we find an empty spot
        while(not_found){
            //only go right if the new datum is strictly greater
            if(temp->datum < new_node->datum){
                if(temp->right){//there's a thing, we keep walking
                    temp = temp->right;
                } else {//we found the spot! GG
                    temp->right = new_node;
                    not_found = false;
                }
            //dupes can go to the left spot
            } else {//temp->datum >= new_datum
                if(temp->left){//there's a thing, we keep walking
                    temp = temp->left;
                } else {//we found the spot! GG
                    temp->left = new_node;
                    not_found = false;
                }
            }
        }
    } else {//root == nullptr: we have our first datum!
        root = new_node;
    }
}

template <class T>
void BinarySortTree<T>::shove_BST_into_Q(std::queue<T> &a_queue){//call recursive
// function to fill the Q in order.
    enqueue_in_order(root, a_queue);
}

template <class T>
void BinarySortTree<T>::enqueue_in_order(node *a_node, std::queue<T> &a_queue){
//output data into queue! in order! OR ELSE!
    if(a_node == nullptr){
        return;
    }
    enqueue_in_order(a_node->left, a_queue);
    a_queue.push(a_node->datum);
    enqueue_in_order(a_node->right, a_queue);
}

template <class T>
void BinarySortTree<T>::nukeBranch(node *a_node){//kill a section of tree
    if(a_node == nullptr){
        return;
    }
    nukeBranch(a_node->left);
    nukeBranch(a_node->right);
    delete a_node;
    //not handling setting things to null
    //caller is responsible for that 'cause I don't need it yet :P
}

#endif // BINARYSORTTREE_H
