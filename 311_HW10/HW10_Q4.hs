hw10q4 :: String -> Double -> IO()
hw10q4 age num = do
  putStr "Price will be $"
  if age == "Child"
   then (do
     putStr (show (num * 7.50))
     print 0
     )
  else if age == "Senior"
   then (do
     putStr (show (num * 8.00))
     print 0
     )
  else (do
     putStr (show (num * 12.50))
     print 0
     )

main = do
  putStrLn "Please enter the type of ticket"
  putStrLn "for ages less than 13: Child"
  putStrLn "for ages from 13 to 65: Adult"
  putStrLn "for ages over 65: Senior"
  age <- getLine
  putStr "How many tickets? "
  parsable_num <- getLine
  let num = (read parsable_num :: Double)
  hw10q4 age num
