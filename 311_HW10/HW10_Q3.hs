hw10q3 :: Int -> Int -> IO()
hw10q3 x y = if x < 13 
   then (do
        putStr (show x)
        putStr " "
        hw10q3 y (x+y)
        )
   else (do
        print 13
        )
