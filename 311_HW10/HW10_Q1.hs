hw10q1 :: [a] -> Integer
hw10q1 [] = 0
hw10q1 (x:xs) = 1 + hw10q1 xs
