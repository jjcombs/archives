hw10q2 :: Eq a => a -> [a] -> Integer
hw10q2 y [] = 0
hw10q2 y (x:xs) = if y == x
  then 1 + hw10q2 y xs
  else hw10q2 y xs
