#include <iostream>
#include <string>
#include "ManyDigits.h"

using namespace std;

void digits_is(string name, ManyDigits *a_num){
	cout << name << "'s value: ";
	a_num->print();
	cout << endl;
}

int main(){
	ManyDigits *a = new ManyDigits();
	digits_is("a", a);
	cout << "prepending 500\n";
	a->HighDigit(500);
	digits_is("a", a);
	cout << "appending 0\n";
	a->lowDigit(0);
	digits_is("a", a);
	cout << "prepending 42\n";
	a->HighDigit(42);
	digits_is("a", a);

	int x = 1337;
	cout << "prepending " << x << endl;
	a->HighDigit(x);
	digits_is("a", a);
	cout << "prepending " << x << endl;
	a->HighDigit(x);
	digits_is("a", a);
	cout << "prepending " << x << endl;
	a->HighDigit(x);
	digits_is("a", a);
	cout << "prepending " << x << endl;
	a->HighDigit(x);
	digits_is("a", a);

	cout << "new number! copy of a! b!\n";
	ManyDigits *b = new ManyDigits(*a);
	digits_is("b", b);

	cout <<"are they equal? ";
	if(*a == *b){
		cout <<"signs point to yes\n";
	}

	cout << "\nnext we'll append 5 to a and 4 to b!\n";
	a->lowDigit(5);
	b->lowDigit(4);
	digits_is("a", a);
	digits_is("b", b);
	ManyDigits c = *a + *b;
	cout << "\nnext we'll see the sum, c!\n";
	digits_is("c", &c);

	cout << "clone of c, d!\n";
	ManyDigits d(c);
	digits_is("d", &d);

	cout << "Clearing d! OH NOES?!\n";
	d.clear();
	digits_is("a", a);
	digits_is("b", b);
	digits_is("c", &c);
	digits_is("d", &d);

	cout << "Clearing b next!\n";
	b->clear();
	digits_is("a", a);
	digits_is("b", b);
	digits_is("c", &c);
	digits_is("d", &d);


	x = 9001;
	cout << "prepending " << x << " to a!\n";
	a->HighDigit(x);
	digits_is("a", a);
	digits_is("b", b);
	digits_is("c", &c);
	digits_is("d", &d);

	cout << "prepending " << x << " to a x1000! for maximum rediculousness!!!!\n";
	for(int i = 0; i < 1000; i++){
		a->HighDigit(x);
	}

	cout <<"\ntest if a is larger than c still xD\n";
	if(*a > c){
		cout <<"a is greater than c! yay!\n";
	} else{
		cout <<"DERP! BYE\n\n";
		return 0;
	}

	cout << "a's number of digits: " << a->getNumDigits() << endl;
	return 0;
}
