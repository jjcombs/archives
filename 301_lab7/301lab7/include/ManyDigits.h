#ifndef MANYDIGITS_H
#define MANYDIGITS_H


class ManyDigits
{
public:
	ManyDigits(); // constructor should setup an empty ManyDigits object.
	virtual ~ManyDigits();
	ManyDigits(const ManyDigits& other_md);// copies other_md into a new ManyDigits object
	int getNumDigits(); //This should return the total number of digits in this object. EX: 555 is three digits
	int HighDigit(int a_num);//This should append the argument n to the ManyDigits object. It should return the new number of digits.
	int lowDigit(int a_num);//This should append the argument n to the ManyDigits object. It should return the new number of digits.
	void clear(); //Clear the object. The number of digits is now 0
	bool isEmpty(); //Return true if the ManyDigits object is empty or clear.
	void print(); //Print the ManyDigits object.

	//overloaded operators:
	ManyDigits& operator=(const ManyDigits& other_md);// too many digits for the basic version
	bool operator>(const ManyDigits& other_md);// too many digits for the basic version of these:
	bool operator<(const ManyDigits& other_md);// too many digits for the basic version of these:
	bool operator==(const ManyDigits& other_md);// too many digits for the basic version of these:
	ManyDigits operator+(const ManyDigits& other_md);// return a new ManyDigits, as expected


private:
	struct FewerDigits{//nodes of ManyDigits
		long long int value;
		FewerDigits *up;
		FewerDigits *down;
	};
	FewerDigits *root; // always points to the node with the ones digit
	FewerDigits *crown; // always points to the node with the most significant digit
	int digits; // number of digits in the number
	// the largest long long int is       9223372036854775807, so we'll compare with
	const static long long int MAX_VALUE =  99999999999999999; // to figure out when to carry to higher order nodes
	const static long long int SUB_VALUE = 100000000000000000; // for converting up to the next higher node
	const static int MAX_WIDTH_OF_FEWER_DIGITS = 17; // for counting digits
//private helper functions
	void push_up_crown(int new_crown_value);//to handle the many instances of adding a new layer above...
	void push_up(FewerDigits *temp, int old_carry);//to recursively deal with the low digit append...

};

#endif // MANYDIGITS_H
