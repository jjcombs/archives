#include "ManyDigits.h"
#include <iostream>


ManyDigits::ManyDigits(){// constructor should setup an empty ManyDigits object.
	//digits is 0 to start (zero is a placeholder)
	digits = 0;
	//have a node with zero from the start
	root = new FewerDigits;
	root->value = 0;
	root->up    = nullptr;
	root->down  = nullptr;
	//crown and root should both point to it at the start
	crown = root;
}

ManyDigits::~ManyDigits(){
	this->clear();
	delete root;
}

ManyDigits::ManyDigits(const ManyDigits& other_md){// copies other_md into a new ManyDigits object
	//set up root...
	this->root = new FewerDigits;
	this->crown = this->root;
	this->root->down  = nullptr;             // root root... get it? :D
	FewerDigits *other_temp = other_md.root; // get a pointer we can traverse with
	//make a copy of root
	this->root->value = other_temp->value;
	//copy each node from the bottom up until we reach the crown (up == nullptr/0)
	while((other_temp = other_temp->up)){
		push_up_crown(other_temp->value);
	}
	this->digits = other_md.digits;
}

int ManyDigits::getNumDigits(){// This should return the total number of digits in this object. EX: 555 is three digits
	return digits;
}

int ManyDigits::HighDigit(int a_num){//This should append the argument n to the ManyDigits object. It should return the new number of digits.
// EX: after running highDigit(1) on the number 555, the new ManyDigits is 1555. This returns 4.
	//if a_num is positive, we'll prepend our number
	if(0 < a_num){
		long long int i = 1;
		while(i <= crown->value){
			i *= 10;//make i large enough to be added to the leftmost digit
		}
		while(i < MAX_VALUE){ //check if it'll fit with all the digits!
			crown->value += i * (a_num % 10);//add a digit on the left end! woo!
			i *= 10;//next digit!
			a_num /= 10;
			digits++;
		}
		if(a_num > 0) {//if there's more to add after filling up this node...
			push_up_crown(a_num);
		}
		//finally, count the new digits
		while(a_num > 0){
			a_num /= 10;
			digits++;
		}
		//and
		return digits;
	}
	//otherwise, throw a fit/exception
	throw std::string("no prepending negative numbers! BAD USER, BAD!");
}

int ManyDigits::lowDigit(int a_num){//This should append the argument n to the ManyDigits object. It should return the new number of digits.
// EX: after running lowDigit(8) on the number 555, the new ManyDigits is 5558. This returns 4.
	//ensure that 0 < a_num
	if(0 < a_num){
		push_up(root, a_num);
		while(a_num > 0){
			a_num /= 10;
			digits++;
		}
		return digits;
	} else if (a_num == 0) {//if a_num == 0 and digits == 0, then we just return 0
		if(digits){//digits > 0 means that we can actually add something!
			int carry;
			FewerDigits *temp = root;
			temp->value *= 10;//append a 0
			do{
				carry = 0;//carry the most significant number up if needed
				while(temp->value > MAX_VALUE){
					carry++;
					temp->value -= SUB_VALUE;
				}
				if(carry){//if something goes up, we might need a new node D:
					if(temp->up){
						temp = temp->up;//maybe we can just add and check again...
						temp->value += carry;
					} else {
						push_up_crown(carry);
						temp = crown;
						carry = 0;//or maybe we need a new node D:
					}
				}
			}while(carry > 0);
			return ++digits;// we'll always be adding exactly one digit in this case
		} else {//appending a zero with a zero does nothing!
			return digits;
		}
	}
	//if a_num < 0 , throw a fit/exception
	throw std::string("no appending negative numbers! BAD USER, BAD!");
}

void ManyDigits::clear(){//Clear the object. The number of digits is now 0
	//delete all nodes that aren't root
	//count down with crown to avoid extra steps :D
	while(crown != root){
		crown = crown->down;
		delete crown->up;
	}
	//don't forget to make root/crown point up at null...
	crown->up = nullptr;
	//set root value to zero
	root->value = 0;
	//and of course,
	digits = 0;
}

bool ManyDigits::isEmpty(){//Return true if the ManyDigits object is empty or clear.
	return 0 == digits; // nifty!
}

void ManyDigits::print(){//Print the ManyDigits object.
	//left to right, start with crown! ftw!
	FewerDigits *temp = crown;
	std::cout << temp->value;
	while((temp=temp->down)){
		//also lolwut, no spaces :D
		std::cout << temp->value;
	}
}

void ManyDigits::push_up(FewerDigits *temp, int old_carry){
	long long int i = 1;
	int carry = 0;
	while(i <= old_carry){
		carry *= 10;//shove digit over!
		i *= 10;
		temp->value *= 10;
		while(temp->value >= MAX_VALUE){//prevent overflow
			carry++;
			temp->value -= SUB_VALUE;
		}
	}
	temp->value += old_carry;
	if(carry){
		if(temp->up){
			push_up(temp->up, carry);
		} else {
			push_up_crown(carry);
		}
	}
}

void ManyDigits::push_up_crown(int new_crown_value){
	//first initialize new crown
	crown->up        = new FewerDigits;
	crown->up->down  = crown;
	crown->up->up    = nullptr;
	crown->up->value = new_crown_value;
	//then move up our crown pointer
	crown            = crown->up;
}

ManyDigits& ManyDigits::operator=(const ManyDigits& other_md){// too many digits for the basic version
	if (this == &other_md){
		return *this; // handle self assignment, <3 code::blocks
	}
	this->clear();//make room for new many digits
	//the rest looks an awful lot like the copy constructor
	FewerDigits *other_temp = other_md.root; // get a pointer we can traverse with
	this->root->value = other_temp->value;
	while((other_temp = other_temp->up)){ //keep going while there's more to copy!
		push_up_crown(other_temp->value);
	}
	this->digits = other_md.digits;
	return *this;//we should return *this, though... fun?
}

bool ManyDigits::operator>(const ManyDigits& other_md){// too many digits for the basic version
	//start with comparing digits
	if(this->digits != other_md.digits){
		return this->digits > other_md.digits;
	}
	//next, compare crown nodes
	if(this->crown->value != other_md.crown->value){
		return this->crown->value > other_md.crown->value;
	}
	//move down until they're not equal
	FewerDigits *our_temp   = this->crown;
	FewerDigits *their_temp = other_md.crown;
	while((our_temp = our_temp->down)){//they're the same length....
		their_temp = their_temp->down;//no need to test both xD
		//once a non-equal part is found, return left_num.value > right_num.value;
		if(our_temp->value != their_temp->value){
			return our_temp->value > their_temp->value;
		}
	}
	//else, if everything down to the root was equal somehow, return false
	return false;
}

bool ManyDigits::operator<(const ManyDigits& other_md){// too many digits for the basic version
	//start with comparing digits... this seems familiar...
	if(this->digits != other_md.digits){
		return this->digits < other_md.digits;
	}
	//next, compare crown nodes
	if(this->crown->value != other_md.crown->value){
		return this->crown->value < other_md.crown->value;
	}
	//move down until they're not equal
	FewerDigits *our_temp   = this->crown;
	FewerDigits *their_temp = other_md.crown;
	while((our_temp = our_temp->down)){//they're the same length....
		their_temp = their_temp->down;//no need to test both xD
		//once a non-equal part is found, return left_num.value < right_num.value;
		if(our_temp->value != their_temp->value){
			return our_temp->value < their_temp->value;
		}
	}
	//else, if everything down to the root was equal somehow, return false
	return false;
}

bool ManyDigits::operator==(const ManyDigits& other_md){// too many digits for the basic version
	//start with comparing digits... this seems familiar...
	if(this->digits != other_md.digits){
		return false;
	}
	//next, compare crown nodes
	if(this->crown->value != other_md.crown->value){
		return false;
	}
	//move down while they're not equal,
	FewerDigits *our_temp   = this->crown;
	FewerDigits *their_temp = other_md.crown;
	while((our_temp = our_temp->down)){//they're the same length....
		their_temp = their_temp->down;//no need to test both xD
		//return false if we don't even make it to the root!
		if(our_temp->value != their_temp->value){
			return false;
		}
	}
	//if we make it to the root and all nodes are individually equal, return true!
	return true;
}

ManyDigits ManyDigits::operator+(const ManyDigits& other_md){// return a new ManyDigits, as expected
	//need a new ManyDigits, COPIED!
	ManyDigits the_sum_of_doom = ManyDigits(other_md);//now we have half the sum!
	//start at root, checking each node for overflow afterwards
	FewerDigits *some_temp = the_sum_of_doom.root;//punny! Also name length!
	FewerDigits *this_temp = this->root;
	some_temp->value += this_temp->value;
	while((this_temp = this_temp->up)){
		if(some_temp == the_sum_of_doom.crown){//pop a new number on top
			the_sum_of_doom.push_up_crown(this_temp->value);
			some_temp = some_temp->up;
		} else {//or we can just add to the number that is there
			some_temp = some_temp->up;
			some_temp->value += this_temp->value;
		}
	}

	//check overflow
	int carry;
	some_temp = the_sum_of_doom.root;
	do{
		carry = 0;//carry the most significant number up if needed
		while(some_temp->value > MAX_VALUE){
			carry++;
			some_temp->value -= SUB_VALUE;
		}
		if(carry){//if something goes up, we might need a new node D:
			if(some_temp->up){
				some_temp = some_temp->up;//maybe we can just add and check again...
				some_temp->value += carry;
			} else {
				the_sum_of_doom.push_up_crown(carry);
				carry = 0;//or maybe we need a new node D:
			}
		}
	}while(carry > 0);

	//fix digits
	some_temp = the_sum_of_doom.root;
	the_sum_of_doom.digits = 0;
	while(some_temp != the_sum_of_doom.crown){
		some_temp = some_temp->up;
		the_sum_of_doom.digits += MAX_WIDTH_OF_FEWER_DIGITS;
	}
	long long int i = the_sum_of_doom.crown->value;
	while(i > 0){
		i /= 10;
		the_sum_of_doom.digits++;
	}

	//ZOMG we can finally return a correct sum.
	return the_sum_of_doom;
}

