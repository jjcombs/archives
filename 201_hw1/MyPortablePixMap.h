#pragma once
#include <string>
#include <iostream>
#include <list>


class MyPortablePixMap
{
    public:
        MyPortablePixMap(std::string magic_word, int width, int height, int max_value);
        virtual ~MyPortablePixMap();

        std::string magic_word()    { return m_magic_word; }
        int width()                 { return m_width; }
        int height()                { return m_height; }
        int max_value()             { return m_max_value; }

        void printTo(std::ostream& destination);
        void readIn(std::istream& source);
        void invertRed();
        void invertGreen();
        void invertBlue();
        void invertAll();
        void setGray();

    protected:

    private:
        void invert(int index);
        std::string m_magic_word;
        int m_width;
        int m_height;
        int m_max_value;
        int m_number_width;
        //save a little space since our array is massive...
        unsigned short int*** m_pixel_array;
};
