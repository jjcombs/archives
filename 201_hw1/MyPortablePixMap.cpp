#include "MyPortablePixMap.h"
#include <iomanip>

using namespace std;

MyPortablePixMap::MyPortablePixMap(string magic_word, int width, int height, int max_value) {
    m_magic_word    = magic_word;
    m_width         = width;
    m_height        = height;
    m_max_value     = max_value;
    //at least one digit and at least one empty space = 2
    m_number_width  = 2;
    //wider numbers need more space:
    while(max_value /= 10){
        m_number_width++;
    }

    //max size is 999 per the hw instructions
    //can't declare the array in the header, so here we go:
    m_pixel_array = new unsigned short int**[999];
    for (int i = 0; i < 999; i++) {
        m_pixel_array[i] = new unsigned short int*[999];
        for (int j = 0; j < 999; j++){
            m_pixel_array[i][j] = new unsigned short int[3];
        }
    }
}

MyPortablePixMap::~MyPortablePixMap() {
    delete[] m_pixel_array;
}

void MyPortablePixMap::printTo(ostream& destination) {
    destination << m_magic_word << endl;
    destination << m_width << ' ' << m_height << endl;
    destination << m_max_value << endl;
    for(int i = 0, j, k; i < m_width; i++){
        j = 0;
        //don't start off with a tab, that's ugly:
        for(k = 0; k < 3; k++){
            destination << setw(m_number_width) << m_pixel_array[i][j][k];
        }
        //do the rest of the row:
        for(j = 1; j < m_height; j++){//we'll worry about non P3 files later, if at all...
            destination << '\t';
            for(k = 0; k < 3; k++){
                destination << setw(m_number_width) << m_pixel_array[i][j][k];
            }
        }
        destination << endl;
    }
}

void MyPortablePixMap::readIn(istream& source){
    // they should all be P3 because they're PPMs, but let's check anyway...
    short int num_in;
    if (m_magic_word == "P3"){
        for(int i = 0, j, k; i < m_width; i++){
            for(j = 0; j < m_height; j++){
                //get all three RGP values:
                for(k = 0; k < 3; k++){
                    source >> num_in;
                    //silently shrink values to maximum and turn negative numbers into zero:
                    num_in = (num_in < 0 ? 0 : (num_in > m_max_value ? m_max_value : num_in));
                    m_pixel_array[i][j][k] = num_in;
                }
            }
        }
    } else {//gray scale or b/w, might want to do something later...
        for(int i = 0; i < m_width; i++){
            for(int j = 0; j < m_height; j++){
                // only one value per pixel, copy 3x to make everything uniform
                source >> num_in;
                //silently shrink values to maximum and turn negative numbers into zero:
                    num_in = (num_in < 0 ? 0 : (num_in > m_max_value ? m_max_value : num_in));
                m_pixel_array[i][j][2] = m_pixel_array[i][j][1] = m_pixel_array[i][j][0] = num_in;
            }
        }
    }
}

void MyPortablePixMap::invert(int index){
    for(int i = 0, j; i < m_width; i++){
        for(j = 0; j < m_height; j++){
            m_pixel_array[i][j][index] = m_max_value - m_pixel_array[i][j][index];
        }
    }
}
void MyPortablePixMap::invertRed()  {invert(0);}
void MyPortablePixMap::invertGreen(){invert(1);}
void MyPortablePixMap::invertBlue() {invert(2);}

void MyPortablePixMap::invertAll(){
    for(int i = 0, j, k; i < m_width; i++){
        for(j = 0; j < m_height; j++){
            //invert all three RGP values:
            for(k = 0; k < 3; k++){
                m_pixel_array[i][j][k] = m_max_value - m_pixel_array[i][j][k];
            }
        }
    }
}

void MyPortablePixMap::setGray(){
    int sum;
    for(int i = 0, j, k; i < m_width; i++){
        for(j = 0; j < m_height; j++){
            //sum:
            sum = 0;
            for(k = 0; k < 3; k++){
                sum += m_pixel_array[i][j][k];
            }
            //find mean
            sum /= 3;
            //put numbers back, not doing fancy things with the magic word and file extension yet...
            for(k = 0; k < 3; k++){
                m_pixel_array[i][j][k] = sum;
            }
        }
    }
}
