#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "MyPortablePixMap.h"

using namespace std;

void testAndFixFilename(string &file_name){
    if(file_name.length() >= 5 && file_name.substr(file_name.length() - 4,4) == ".ppm"){
        //seems like a legit filename
    }else{
        cout <<'"' << file_name << "\" is missing the ppm suffix, adjusting to \"";
        file_name += ".ppm"; //they forgot the .ppm!
        cout << file_name << '"' << endl;
    }
}

void openFile(string &file_name, ofstream &file_stream){
    testAndFixFilename(file_name);

    ifstream test;
    test.open(file_name.c_str());
    if( test.is_open()){
        int i = 0;
        string desired_name = file_name.substr(0, file_name.length() - 4);
        do{
            test.close();
            test.open(desired_name + to_string(i++) + ".ppm");
        }while(test.is_open());
        file_name = desired_name + to_string(i) + ".ppm";
        cerr << "Warning: file \"" << desired_name << ".ppm\" exists,"
            <<" changing output file name to " << file_name
            << " instead of overwriting!\n";
    }
    //let ifstream test close via destructor, for the win!

    //open the non-existant file...
    file_stream.open(file_name.c_str());
}
void openFile(string &file_name, ifstream &file_stream){
    testAndFixFilename(file_name);

    file_stream.open(file_name.c_str());
    //exit and complain if we can't find the file:
    if(!file_stream.is_open()){
        cerr << "Error: input file not found, exiting!\n";
        exit(0);
    }
}

int showMenuAndGetInput(const string& input_file_name){
    int user_input, choice_num = 1;
    do{
        cout << "\n\nImage Processing choices for \"" << input_file_name << "\":\n\n";
        cout << choice_num++ << ". Convert to grayscale\n";
        cout << choice_num++ << ". Invert Red\n";
        cout << choice_num++ << ". Invert Green\n";
        cout << choice_num++ << ". Invert Blue\n";
        cout << choice_num++ << ". Invert All\n";
        cout <<  "\nEnter choice (0 to quit): ";
        cin >> user_input;
        if(user_input >= 0 && user_input < choice_num){
            return user_input;
        }
        cout << "\n\nSorry, that was not a valid choice, please try again:\n";
    }while(true);//get valid input, or else!
}

int main() {
    string in_file_name;
    cout << "Enter the input file name:";
    cin >> in_file_name;

    ifstream input_ppm;
    openFile(in_file_name, input_ppm);



    //read the header first:
    string magic_word; //P1 == B/W, P2 == grayscale, P3 == RGB
    int width,
        height,
        max_value;
    input_ppm >> magic_word
            >> width
            >> height
            >> max_value;

    //only continue if the size fits!
    if (width < 1000 && height < 1000){
        string out_file_name;
        cout << "Enter the output file name:";
        cin >> out_file_name;
        //we can move these two down later if we want .pbm or .pgm
        ofstream output_ppm;
        openFile(out_file_name, output_ppm);

        //create something to represent our ppm:
        MyPortablePixMap ppm = MyPortablePixMap(magic_word, width, height, max_value);
        ppm.readIn(input_ppm);
        input_ppm.close();

        int chosen_option = showMenuAndGetInput(in_file_name);
        bool option_was_okay = true;
        switch(chosen_option){
        case 1:
            ppm.setGray();
            break;
        case 2:
            ppm.invertRed();
            break;
        case 3:
            ppm.invertGreen();
            break;
        case 4:
            ppm.invertBlue();
            break;
        case 5:
            ppm.invertAll();
            break;
        case 0: //0 means just quit, let's give them an awkward goodbye! :D
            cout << "\n...\n...okay then...\n...\n";
            option_was_okay = false;
            break;
        default:
            cout << "That option wasn't implemented, sorry about that...\n...\n";
            option_was_okay = false;
        }
        if(option_was_okay){//save the modified ppm!
            ppm.printTo(output_ppm);
            cout << endl << '"' << out_file_name << "\" was successfully created!\n\n";
            //let the destructor close output_ppm
        }
        cout << "Bye!\n";
    } else { //exit and let the user know if the ppm doesn't fit inside a 999x999 area
        cout << "Sorry, this program isn't meant to handle files that are over 999 pixels wide"
             << endl << "or over 999 pixels tall." << endl
             << "Exiting without accomplishing anything.\n\n...Bye!\n"
             << endl;
    }

    return 0;
}
