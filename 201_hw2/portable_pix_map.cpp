#include "portable_pix_map.h"
#include <iomanip>

using namespace std;

portable_pix_map::portable_pix_map(string magic_word, int width, int height, int max_value) {
	m_magic_word = magic_word;
	m_width      = width;
	m_height     = height;
	m_max_value  = max_value;//technically should always be 255
	//at least one digit and at least one empty space = 2
	m_number_width  = 2;//technically should always end up as 5
	//wider numbers need more space:
	while(max_value /= 10){
		m_number_width++;
	}
	m_num_files = 0;
	array_initializer();
}

void portable_pix_map::array_initializer() {
	//can't declare the array in the header, so here we go:
	m_pixel_array = new unsigned short int**[m_width];
	for (int i = 0; i < m_width; i++) {
		m_pixel_array[i] = new unsigned short int*[m_height];
		for (int j = 0; j < m_height; j++){
			m_pixel_array[i][j] = new unsigned short int[3];
			for(int k = 0; k < 3; k++){
				m_pixel_array[i][j][k] = 0;
			}
		}
	}
}

portable_pix_map::~portable_pix_map() {
	for (int i = 0; i < m_width; i++) {
		for (int j = 0; j < m_height; j++){
			delete[] m_pixel_array[i][j];
		}
		delete[] m_pixel_array[i];
	}
    delete[] m_pixel_array;
}

void portable_pix_map::print_with_tabs_to(ostream& destination) {
	if(m_num_files < 1){
		throw string("Error making file: nothing read in yet!");
	}
	destination << m_magic_word << endl;
	destination << m_width << ' ' << m_height << endl;
	destination << m_max_value << endl;
	for(int i = 0, j; i < m_width; i++){
		// m_height - 1 to set up the extra tab avoidance later
		for(j = 0; j < m_height - 1; j++){
			for(int k = 0; k < 3; k++){//ppm means three colors: R G B
				destination << setw(m_number_width)
						<< m_pixel_array[i][j][k] / m_num_files;
			}
			destination << '\t';
		}
		// don't put an extra tab at the end:
		for(int k = 0; k < 3; k++){//ppm means three colors: R G B
			destination << setw(m_number_width)
					<< m_pixel_array[i][j][k] / m_num_files;
		}
		destination << endl;
	}
}

void portable_pix_map::print_to(ostream& destination) {
	if(m_num_files < 1){
		throw string("Error making file: nothing read in yet!");
	}
	destination << m_magic_word << endl;
	destination << m_width << ' ' << m_height << endl;
	destination << m_max_value << endl;
	for(int i = 0, j; i < m_width; i++){
		for(j = 0; j < m_height; j++){
			for(int k = 0; k < 3; k++){//ppm means three colors: R G B
				//just endline after every number to make a smaller file
				destination << m_pixel_array[i][j][k] / m_num_files << endl;
			}
		}
	}
}

void portable_pix_map::read_in(istream& source){
	// they should all be P3 because they're PPMs, but let's check anyway...
	short int num_in;
	string magic_word;
	source >> magic_word;//check/toss "P3"
	if(magic_word != m_magic_word){
		throw string("Error reading file: unexpected magic word!");
	}
	source >> num_in;//check/toss width
	if(num_in != m_width){
		throw string("Error reading file: unexpected width!");
	}
	source >> num_in;//check/toss height
	if(num_in != m_height){
		throw string("Error reading file: unexpected height!");
	}
	source >> num_in;//check/toss value
	if(num_in != m_max_value){
		throw string("Error reading file: unexpected maximum pixel value!");
	}
	if (m_magic_word == "P3"){
		for(int i = 0, j, k; i < m_width; i++){
			for(j = 0; j < m_height; j++){
				//get all three RGP values:
				for(k = 0; k < 3; k++){
					source >> num_in;
					//silently shrink values to maximum and turn negative numbers into zero:
					num_in = (num_in < 0 ? 0 : (num_in > m_max_value ? m_max_value : num_in));
					m_pixel_array[i][j][k] += num_in;
				}
			}
		}
	}//gray scale or b/w, might want to do something later...
	m_num_files++;
}
