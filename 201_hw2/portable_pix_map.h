#pragma once
#include <string>
#include <iostream>
#include <list>


class portable_pix_map
{
public:
	portable_pix_map(std::string magic_word, int width, int height, int max_value);
	virtual ~portable_pix_map();

	std::string magic_word() { return m_magic_word; }
	int width()              { return m_width; }
	int height()             { return m_height; }
	int max_value()          { return m_max_value; }

	void print_with_tabs_to(std::ostream& destination);
	void print_to(std::ostream& destination);
	void read_in(std::istream& source);

protected:

private:
	void array_initializer();

	std::string m_magic_word;
	int m_width;
	int m_height;
	int m_max_value;
	int m_number_width;
	int m_num_files;
	//save a little space with short int since our array is massive...
	//it's fine as long as we don't need to combine over 256 images
	unsigned short int*** m_pixel_array;
};
