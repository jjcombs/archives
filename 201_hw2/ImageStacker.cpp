#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "portable_pix_map.h"

using namespace std;

int main() {
	//4 variables for header info:
	string magic_word; //P1 == B/W, P2 == grayscale, P3 == RGB
	int width,
		height,
		max_value;
	// a couple of strings to help juggle file names:
	string file_name;
	string file_beginning;
	// the ifstream with which we'll read everything!
	ifstream input_ppm;
	int i;
	for(i = 0; i < 3; i++){// three tries to get it right
		cout << "Please enter the the name of the picture set:\n";
		cout << "(It should match the name of the directory)\n";
		cin >> file_name;
		cout << "Please enter the absolute or relative path to the directory";
		cout << " that contains the\npicture set folder:\n";
		cout << "(use './' for the current directory)\n";
		cin >> file_beginning;
		if('/' != file_beginning[file_beginning.size()-1]){
			//add a '/' if the user forgets it
			file_beginning += '/';
		}
		//test if the filename works out...
		file_beginning = file_beginning + file_name + '/' + file_name + '_';
		file_name = file_beginning  + "001.ppm";
		input_ppm.open(file_name.c_str());
		// if everything is fine, load header info and stop asking
		if(input_ppm.is_open()){
			input_ppm >> magic_word
					>> width
					>> height
					>> max_value;
			input_ppm.close();
			break;
		}
		// otherwise yell at the user, telling him to try again
		cout << "\n\nUnable to open '" << file_name << "'\n";
		if(i == 2){
			cout << "\nAfter " << i+1 << " failed tries, I give up!\nBYE!\n";
			return 0; //exit/give up at life
		}
		cout << "Please try again.\n\n";
	}

	portable_pix_map accumulator_ppm = portable_pix_map(magic_word, width, height, max_value);

	// set_name has correct value already:
	//set_name = file_beginning + "001.ppm";
	// same as in the loop!
	input_ppm.open(file_name.c_str());

	for(i = 2; i < 10 && input_ppm.is_open(); i++){
		accumulator_ppm.read_in(input_ppm);
		input_ppm.close();
		file_name = file_beginning + "00" + to_string(i) + ".ppm";
		input_ppm.open(file_name.c_str());
	}
	for(; i < 100 && input_ppm.is_open(); i++){
		accumulator_ppm.read_in(input_ppm);
		input_ppm.close();
		file_name = file_beginning + '0' + to_string(i) + ".ppm";
		input_ppm.open(file_name.c_str());
	}
	//i == 1000 gurantees that the file won't read, so we'll be done closing
	for(; i <= 1000 && input_ppm.is_open(); i++){
		accumulator_ppm.read_in(input_ppm);
		input_ppm.close();
		file_name = file_beginning + to_string(i) + ".ppm";
		input_ppm.open(file_name.c_str());
	}
	file_name = file_beginning + "good.ppm";

	ofstream output_file = ofstream(file_name.c_str());
	accumulator_ppm.print_to(output_file);

	return 0;
}
